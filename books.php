<?php
    include "db.php";
    include "query.php";
?>
<html>
    <head>
    <title>Object Oriented PHP class 4 db</title>
    </head>
    <body>
        <p>
        <?php
            $db = new DB('localhost','intro','root','');
            $dbc = $db->connect();
            $query = new Query($dbc);
            $q = "SELECT b.title, u.name
                  FROM books b JOIN users u ON b.user_id=u.id";
            $result = $query->query($q);
            echo '<br>';
            if($result->num_rows > 0){
                echo '<table>';
                echo '<tr><th>Book id</th><th>Author</th><th>Title</th><th>User Name</th></tr>';
                while($row = $result->fetch_assoc()){
                    echo '<tr>';
                    echo '<td>' .$row['title'].'</td><td>' .$row['name'].'</td>';
                    echo '</tr>';
                }
                echo '</table>';
            }
            else {
                echo "Sorry no results";
            }
        ?>
        </p>
    </body>
</html>