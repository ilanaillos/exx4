<?php
class DB{
    protected $db_name;
    protected $db_user;
    protected $db_password;
    protected $db_host;
    function __construct($db_host,$db_name,$db_user,$db_password){
        $this->db_host = $db_host;
        $this->db_name = $db_name;
        $this->db_user = $db_user;
        $this->db_password = $db_password;
    }
   
    public function connect(){
        $this->db = new mysqli($this->db_host,$this->db_user,$this->db_password,$this->db_name); 
        if(mysqli_connect_errno()){ 
            printf("Connection failed %s", mysqli_connect_error());
        }
        return $this->db;
        
    }
}





?>